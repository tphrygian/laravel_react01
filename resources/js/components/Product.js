import React, { Component} from 'react';
import ReactDOM from 'react-dom';

class Product extends Component {
    render() {
        return (
            <div className='container'>
                <div className='image'>
                    <img src='images/products/image-aqua.png' />
                </div>
                <div className='middle aligned content'>
                    <div className='description'>
                        <a>Fort Knight</a>
                        <p>Authentic renaissance actors, delivered in just two weeks.</p>
                    </div>
                    <div className='extra'>
                        <span>Submitted by:</span>
                        <img className='ui avatar image' src='images/avatars/daniel.jpg' />
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;
if (document.getElementById('product')) {
    ReactDOM.render(<Product />,
        document.getElementById('product'));
}
