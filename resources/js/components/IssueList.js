import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const issues = [
    {
        id: 1, status: 'New', owner: 'Ravan', effort: 5,
        created: new Date('2018-08-15'), due: undefined,
        title: 'Error in console when clicking Add',
    },
    {
        id: 2, status: 'Assigned', owner: 'Eddie', effort: 14,
        created: new Date('2018-08-16'), due: new Date('2018-08-30'),
        title: 'Missing bottom border on panel',
    },
];

class IssueFilter extends Component {
    render () {
        const issue = this.props.issue;
        return (
            <div>This is a placeholder for the issue filter.</div>
        );
    }
}

class IssueRow extends React.Component {
    render() {
        const style = this.props.rowStyle;
        return (
            <tr>
                <td style={style}>{this.props.issue_id}</td>
                <td style={style}>{this.props.issue_title}</td>
            </tr>
        );
    }
}

class IssueTable extends Component {
    render() {
        const rowStyle = {border: "1px solid silver", padding: 4};
        return (
            <div>
                <table style={{borderCollapse: "collapse"}}>
                    <thead>
                    <tr>
                        <th style={rowStyle}>ID</th>
                        <th style={rowStyle}>Title</th>
                    </tr>
                    </thead>
                    <tbody>
                        <IssueRow rowStyle={rowStyle} issue_id={1}
                                  issue_title="Error in console when clicking Add" />
                        <IssueRow rowStyle={rowStyle} issue_id={2}
                                  issue_title="Missing bottom border on panel" />
                    </tbody>
                </table>
            </div>
        );
    }
}

class IssueAdd extends Component {
    render() {
        return (
            <div>This is placeholder for a form to add an issue.</div>
        );
    }
}

class BorderWrap extends Component {
    render() {
        const borderStyle = {border: "1px solid silver", padding: 6};
        return (
            <div style={borderStyle}>
                {this.props.children}
            </div>
        );
    }
}

class IssueList extends Component {
    render() {
        return (
            <div className='container'>
                <React.Fragment>
                    <h1>Issue Tracker</h1>
                    <IssueFilter />
                    <hr />
                    <IssueTable />
                    <hr />
                    <IssueAdd />
                </React.Fragment>
            </div>
        );
    }
}

export default IssueList;

const element = <IssueList />;
ReactDOM.render(
    element,
    document.getElementById('root'));
