import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Hello extends Component {
    render() {
        const continents = ['Africa','America','Asia','Australia','Europe'];
        const helloContinents = Array.from(continents, c => `Hello ${c}!`);
        const message = helloContinents.join(' ');

        return (
            <div className='container'>
                <h1>{message}</h1>
            </div>
        );
    }
}
export default Hello;

if (document.getElementById('root')) {
    ReactDOM.render(
        <Hello />,
        document.getElementById('root')
    );
}
